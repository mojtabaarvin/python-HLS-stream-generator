import subprocess
import uuid
import os

from bson import ObjectId
from flask import Blueprint, request, current_app, jsonify, url_for, \
    make_response
from os.path import splitext, basename
from furl import furl


import exc
from extensions import fs
from tasks import video_converter,save_video_from_url
from pprint import pprint

mod = Blueprint('views', __name__)





def save_video_from_form(file):
    orig_filename, file_ext = splitext(basename(file.filename))
    print(f'orig_filename = {orig_filename}\n')
    print(f'file_ext = {file_ext}\n')
    saved_filename = str(uuid.uuid4()) + file_ext
    print(f'saved_filename = {saved_filename}\n')
    source_filepath = current_app.config['MEDIA_FOLDER'] + saved_filename
    print(f'source_filepath = {source_filepath}\n')
    file.save(source_filepath)
    print(f'file saved\n')
    return source_filepath

def get_webhook(webhook_encode):
    if not webhook_encode:
        raise exc.WebhookRequiredException

    webhook = furl(webhook_encode)
    webhook_path = webhook.origin + webhook.pathstr.rstrip('/')
    if webhook_path not in current_app.config['WEB_HOOKS']:
        raise exc.WebhookNotValidException

    return webhook

@mod.route('/upload', methods=['GET', 'POST'])
def get_video():
    print('\x1b[6;30;42m' + 'request = '+ '\x1b[0m'+ f' {request}\n' )
    # TODO : agar fasle beyne requestha kam boud , hamoun task_id ghabli ra bedahad.
    print(f"current_app= {current_app}")
    if request.method == 'POST':
        # ############ POST METHOD ############
        if 'file' not in request.files:
            raise exc.FileKeyNotFoundException

        webhook = get_webhook(request.form.get('webhook'))

        watermark = request.form.get('watermark')
        if watermark and watermark not in ['tr', 'tl', 'br', 'bl']:
            raise exc.WatermarkIsNotValidException

        base_filename = request.files['file'].filename
        filename = save_video_from_form(request.files['file'])
    else:
        # ############ GET METHOD ############
        furl_obj = furl(request.args.get('url'))
        print(f'furl_obj = {furl_obj}\n')
        url = furl_obj.url
        print(f'url = {url}\n')
        filepath = furl_obj.path
        print(f'filepath = {filepath}\n')
        base_filename = basename(str(filepath))
        print(f'base_filename = {base_filename}\n')
        if not url:
            raise exc.UrlArgNotFoundException

        webhook = get_webhook(request.args.get('webhook'))
        print(f'webhook = {webhook}\n')

        watermark = request.args.get('watermark')
        print(f'watermark = {watermark}\n')

        if watermark and watermark not in ['tr', 'tl', 'br', 'bl']:
            raise exc.WatermarkIsNotValidException

        task = save_video_from_url.apply_async(
                                        kwargs=dict(
                                            url=url,
                                            base_filename=base_filename,
                                            watermark=watermark,
                                            client_ip=request.remote_addr,
                                            webhook=webhook.url,
                                            config_url_cache=current_app.config['URL_CACHE'],
                                            config_media_folder=current_app.config['MEDIA_FOLDER']
                                            )
                                        )
        
        print(f'task = {task}\n')
        print(f'task.task_id = {task.task_id}\n')
    return jsonify(
        {
            'status': 'ACCEPTED',
            'result': {
                'task_id': task.task_id,
                '_link': {
                    'progress': url_for('views.task_status',
                                        task_id=task.task_id,
                                        _external=True),
                }
            }
        }), 202

        # print(f'filename = {filename}\n')

    # task = video_converter.apply_async(kwargs=dict(
    #     base_filename=base_filename,
    #     input_file=filename,
    #     watermark=watermark,
    #     client_ip=request.remote_addr,
    #     webhook=webhook.url
    # ))
    # print(f'task = {task}\n')
    # print(f'task.task_id = {task.task_id}\n')
    # return jsonify(
    #     {
    #         'status': 'ACCEPTED',
    #         'result': {
    #             'task_id': task.task_id,
    #             '_link': {
    #                 'progress': url_for('views.task_status',
    #                                     task_id=task.task_id,
    #                                     _external=True),
    #             }
    #         }
    #     }), 202

@mod.route('/status/<task_id>', methods=['HEAD'])
def task_status(task_id):
    print("task_status")
    task = video_converter.AsyncResult(task_id)
    pprint(task)
    if task.state == 'PENDING':
        response = {
            'X-State': task.state,
            'X-Percent': 0,
            'X-Status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'X-State': task.state,
            'X-Percent': task.info.get('percent', 0),
            'X-Status': task.info.get('status', '')
        }
        if 'result' in task.info:
            response['X-Result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'X-State': task.state,
            'X-Percent': 1,
            'X-Status': str(task.info),  # this is the exception raised
        }
    return '', 204, response

@mod.route('/pull/<file_id>')
def pull_file(file_id):
    if current_app.config['ONLY_PULL_FROM_SERVER_HOST'] and \
                    current_app.config['SERVER_HOST'] != request.host:
        raise exc.ServerHostIsNotValidException

    key = request.args.get('key')
    if not key:
        return jsonify({'error': '`key` argument is requirement'}), 400

    grid_out = fs.find_one({'_id': ObjectId(file_id), 'key': key})
    if grid_out is None:
        raise exc.FileNotFoundException

    response = make_response(grid_out.read())
    mimtype_dict = {
        'video': 'video/mp4',
        'screenshot': 'image/jpeg'
    }
    response.mimetype = mimtype_dict[grid_out._file['type']]
    response.headers['Content-Disposition'] = 'attachment'

    return response
